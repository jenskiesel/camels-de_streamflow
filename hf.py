import re


def clean_string(s):
    #return re.compile('\W+').sub('', s).strip()
    #return re.sub('[\W_]+', '', s)
    k = "1234567890abcdefghijklmnopqrstuvwxyzäöüABCDEFGHIJKLMNOPQRSTUVWXYZÄÖÜ"
    getVals = list(filter(lambda x: x in k, s))
    return "".join(getVals)



def print_progress_bar_intervals(iteration, total, lastPrint=None, totalPrints=None, prefix='', suffix='',
                       decimals=1, length=100, fill='|', printEnd = "\r", type='bar', ):
    """
    Call in a loop to create terminal progress bar for a desired number of outputs
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        length      - Optional  : character length of bar (Int)
        fill        - Optional  : bar fill character (Str)
        printEnd    - Optional  : end character (e.g. "\r", "\r\n") (Str)
        type        - Optional  : 'bar' is the progress bar and 'pct' is a number with percent only
        stepType    - Optional  : None: each call prints a progress bar,
                                  'time': each passed stepPrint in min prints output
                                  'pct': echa passed stepPrint in percent prints output
        lastPrint   - Optional  : the last iteration a printout was done
        totalPrints - Optional  : the total number of printouts desired (None if all)
        returns current print which has to be passed to the function as lastPrint on next iteration
    """
    printout = False
    if totalPrints is None:
        printout = True
    elif iteration - lastPrint > total / totalPrints or (lastPrint == 0 or iteration == total):
        printout = True
    if printout:
        percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
        if type == 'pct':
            print(percent, end = printEnd, flush=True)
        elif type == 'bar':
            filledLength = int(length * iteration // total)
            bar = fill * filledLength + '-' * (length - filledLength)
            print(f'\r{prefix} |{bar}| {percent}% {suffix}', end=printEnd)
            print(f'\r{prefix} |{bar}| {percent}% {suffix}', end='')  # Print New Line on Complete
            # https://stackoverflow.com/questions/34950201/pycharm-print-end-r-statement-not-working
            if iteration == total:
                print()
        return iteration
    else:
        return lastPrint


def print_progress_bar(iteration, total, prefix = '', suffix = '', decimals = 1, length = 100, fill = '█', printEnd = "\r"):
    """
    Call in a loop to create terminal progress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        length      - Optional  : character length of bar (Int)
        fill        - Optional  : bar fill character (Str)
        printEnd    - Optional  : end character (e.g. "\r", "\r\n") (Str)
    """
    percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
    filledLength = int(length * iteration // total)
    bar = fill * filledLength + '-' * (length - filledLength)
    print(f'\r{prefix} |{bar}| {percent}% {suffix}', end = printEnd)
    print(f'\r{prefix} |{bar}| {percent}% {suffix}', end='')  # https://stackoverflow.com/questions/34950201/pycharm-print-end-r-statement-not-working
    # Print New Line on Complete
    if iteration == total:
        print()
