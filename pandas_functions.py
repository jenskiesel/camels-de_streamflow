import pandas as pd
import numpy as np
import sys

# iterate over rows dataframe

# pandas datetime format code
# https://docs.python.org/3/library/datetime.html#strftime-and-strptime-behavior

# setting with copy warning when slicing a dataset and then working on the slice
#df_sub = df[(df['name'] == 'aname')].copy(deep=False)

# select first row of column 'id'
# df['id'].iloc[0]

# get minimum/maximum without dropping cols
#df_min = df.loc[df.groupby(grp_col)[cols_for_min].idxmin()]

# select column and row based on colnames and index
# df.loc[0:['id','name']]

# select column based on colname
# df.loc[:['name']]

# access first column regardless of column name
# df.iloc[:,0]

# access first row regardless of index
# df.iloc[0,:]

# subset dataframe
#df = df[(df['station']==station_id)]
#df = df[(df['gold']>0) & (df['silver']>0)] and
#df = df[(df['gold']>0) | (df['silver']>0)] or
# df = df[df['month'].str.contains('Ju|Ma')]  # substring in string with OR connection
# df = df[~df['month'].str.contains('Ju|Ma')]  # substring - NOT in string
# df_sub = df_sub[df_sub['col'].isin(list)]  # if in list


# replaces the values in "column" based on a replace dict
# df[column] = df[column].replace(replace_dict)

# apply where condition
# df['date'] = np.where(df['date'].dt.year > 2033, df['date'] + pd.DateOffset(years=-100), df['date'])

# concatenate columns to one string
#df['combination'] = df[['col1', 'col2']].astype(str).apply(lambda x: '_'.join(x), axis=1)

# group dataframe and calculate statistics for groups:
#df_grp = df.groupby(['group_var', 'year']).agg({'value': ['median', 'min', 'max']})
#df_grp = df_sub.groupby(['seperate_by'])['value'].quantile(np.divide(c.PERCENTILES, 100))
#cols_to_agg = list(df_sub.columns.drop(c.COMBINE_BY + ['value']))
#df_sub = df_sub.groupby(cols_to_agg).agg({'value': 'mean'}).reset_index()

# group dataframe and calculate percentiles for groups !AND keep all columns!
# https://stackoverflow.com/questions/17578115/pass-percentiles-to-pandas-agg-function

# count nans in column
#nr_nans = df.isna().sum()

# apply function to part of dataframe:
#for dep_var in dep_vars:
#    df = df.groupby(groupid_col).apply(lambda x: function(x, df_col))
#
#def function(df, col):
#    """
#    normalize the column's min to 0 and max to 1
#    :param df: the dataframe
#    :param col: the name of the column to be normalized
#    :return: original dataframe with normalized column
#    """
#    df[col] = (df[col] - min(df[col])) / (max(df[col]) - min(df[col]))
#    return df
#
#sort dataframe
# cols_to_sort = ['col1', 'col2', 'col3']
# ascending = [True for _ in cols_to_sort]
# df = df.sort_values(cols_to_sort, ascending=ascending)
#
#
#append dataframes with same columns
#df_sub = pd.concat([df_sub, df1])
#
#
#
# return all columns for groupby min, median (or other percentile), max of 'value'
#df_min = df_sub.loc[df_sub.groupby('seperate_by')['value'].idxmin()]
#df_max = df_sub.loc[df_sub.groupby('seperate_by')['value'].idxmax()]
#df_med = df_sub.loc[df_sub.groupby('seperate_by').apply(get_closest_percentile_index, 'value', 0.5)]

def get_closest_percentile_index(df, col, percentile):
    d = df[col]
    ranks = d.rank(pct=True)
    close_to_percentile = abs(ranks - percentile)
    return close_to_percentile.idxmin()

def quantile(df, col_str, q):
    return df[col_str].quantile(q)

def correct_y2k_problem(df, dt_col):
	df[dt_col] = np.where(df[dt_col].type.year > 2033, df[dt_col] + pd.DateOffset(years=-100), df[dt_col])


def to_fwf(df, fname):
    from tabulate import tabulate
    content = tabulate(df.values.tolist(), list(df.columns), tablefmt="plain")
    open(fname, "w").write(content)

def clean_timeseries_df(df, index_col, datetime_format, data_cols, freq, max_n_interpol, add_nodata=None,
                        data_as_numeric=False):
    """
    cleaning a pandas time series
    - making sure consecutive date/time exists in index
    - if data_as_numeric is true: converts the data columns to float and sets all non floats to no data
    - filling all missing dates with nan
    - interpolating nans with length <= max_n_interpol
    - make index_col the index and set name of index to index_col
    :param df: pandas dataframe without index
    :param index_col: index column as string
    :param datetime_format: format of data
    :param data_cols: list of data columns as string
    :param freq: the time step frequency according to https://pandas.pydata.org/pandas-docs/stable/user_guide/timeseries.html#timeseries-offset-aliases
                 H = hourly frequency
                 D = calendar day
                 W = weekly
                 M = month END frequency
                 SM = semi month end (15th and end)
                 MS = month start
    :param max_n_interpol: the number of time steps filled with linear interpolation post resampling
    :param add_nodata: list of additional no data values to be replaced with np.NaN
    :return:
    """
    if add_nodata is not None:
        df = df.replace(add_nodata, np.NaN)
    df = set_and_sort_index(df, index_col, datetime_format)
    if df is None:
        return None
    if data_as_numeric:
        for data_col in data_cols:
            df[data_col] = pd.to_numeric(df[data_col], errors='coerce')
    df_min_date = df.index.min()
    df_max_date = df.index.max()
    df = df.reindex(pd.date_range(str(df_min_date), str(df_max_date), freq=freq), fill_value=np.NaN)
    df.index.name = index_col
    if max_n_interpol > 0:
        for data_col_str in data_cols:
            df[data_col_str] = df[data_col_str].interpolate(method='time',
                                                            axis=0,
                                                            limit=max_n_interpol)  # interpolate missing values
    return df

def remove_nan_at_beginning_end(df, data_cols, remove_type='inner'):
    """
    function removes the nans at the beginning and end of a dataframe
    for the 'data_cols' based on the 'remove_type'
    the df must have an index defined
    :param df: pandas dataframe
    :param data_cols: list of strings with column names
    :param type: 'inner' = rows are kept where all data_cols have data at beginning/end
                           (least data returned, but all data_cols have data at beginning and end)
                 'outer' = rows are kept where one data_col has data at beginning/end
                           (max data returned, but some cols may still have nan at beginning and end)
    :return:
    """
    # get the first and last valid index of the first data column
    min_idx = df[data_cols[0]].first_valid_index()
    max_idx = df[data_cols[0]].last_valid_index()
    # iterate through the remaining data columns - NOT TESTED
    for data_col in data_cols[1:]:
        first_idx = df[data_col].first_valid_index()
        last_idx = df[data_col].last_valid_index()
        # set min and max idx depending on the 'inner' and 'outer' criteria
        if remove_type == 'inner':
            min_idx = max(min_idx, first_idx)
            max_idx = min(max_idx, last_idx)
        elif remove_type == 'outer':
            min_idx = min(min_idx, first_idx)
            max_idx = max(max_idx, last_idx)
        else:
            raise ValueError(f'{remove_type} not defined - only "inner" and "outer" possible')
    return df.loc[min_idx:max_idx]


def set_and_sort_index(df, index_col, datetime_format):
    try:
        df.set_index(index_col, inplace=True)
        df.index = pd.to_datetime(df.index, format=datetime_format)
        df.sort_index(inplace=True)
        return df
    except:
        # occurs if date format is different
        return None


def subset_period(df, period_start, period_end):
    mask = (df.index >= period_start) & (df.index <= period_end)
    return df.loc[mask]

def merge_on_index(df1, df2):
    return df1.merge(df2, how='outer', left_index=True, right_index=True)

def get_nr_missing_in_ts(df, index_col, datetime_format, freq):
    """
    checks if the time series of the df is consecutive and
    returns the number of missing time steps
    """
    df2 = set_and_sort_index(df, index_col, datetime_format)
    df_min_date = df2.index.min()
    df_max_date = df2.index.max()
    df2 = df2.reindex(pd.date_range(str(df_min_date), str(df_max_date), freq=freq), fill_value=np.NaN)
    return len(df2) - len(df)

def get_percent_nan(df, col_str):
    if len(df) > 0:
        nr_nans = df[col_str].isnull().sum()
        return (nr_nans / len(df)) * 100
    else:
        return 100



def normalize_min_max_0_1(df, col):
    """
    normalize the column's min to 0 and max to 1
    :param df: the dataframe
    :param col: the name of the column to be normalized
    :return: original dataframe with normalized column
    """
    df[col] = (df[col] - min(df[col])) / (max(df[col]) - min(df[col]))
    return df


def multi_to_single_column_index(df, sep='_'):
    df.columns = [sep.join(col).strip() for col in df.columns.values]
    return df

def statistics_from_series(series, stats_str):
    """
    stats_str = 'mean', 'min', 'max', 'pxx' where xx is the percentile
    :param series:
    :param stats_str:
    :return:
    """
    if stats_str == 'mean':
        return series.mean()
    elif stats_str == 'min':
        return series.min()
    elif stats_str == 'max':
        return series.max()
    elif stats_str.startswith('p'):
        q = float(stats_str[1:])/100
        return series.quantile(q)

def assign_rise_fall(df, col, type, force_same_length):
    """
    function assigns 'rise' to the rising part of the df[col] series and 'fall'
    to the falling part of the df[col] series. The rising part is defined as time steps
    prior to the maximum, falling part is defined as time steps post the maximum
    df must have a datetime index
    :param df: dataframe to which column 'limb_type' is added
    :param col: the variable for which rising and falling is detected
    :param type: 'fall' or 'rise' indicate which part of the events are returned
    :param force_same_length: 'fall' and 'rise' will have the same length
    :return: df
    """
    max_ts_lst = list(df.index[df[col]==max(df[col])])
    max_ts = max_ts_lst[int(len(max_ts_lst)/2)]  # get middle point in case multiple maximums exist
    df['type'] = np.NaN
    if force_same_length:
        # find number of event-steps pre and post the maximum and select the minimum of the two
        min_n = sys.maxsize
        df['type'] = df['type'].where(df.index > max_ts, 'rise')
        min_n = min(df[df['type'] == 'rise']['type'].count(), min_n)
        df['type'] = df['type'].where(df.index < max_ts, 'fall')
        min_n = min(df[df['type'] == 'fall']['type'].count(), min_n)
        df['type'] = np.NaN
        # find the min and max time step
        freq = pd.infer_freq(df.index)
        delta_time = pd.Timedelta(pd.tseries.frequencies.to_offset(freq) * (min_n - 1))
        start_rise_ts = max_ts - delta_time
        end_fall_ts = max_ts + delta_time
        if type == 'rise':
            df['type'] = df['type'].where((df.index > max_ts) | (df.index < start_rise_ts), 'rise')
        elif type == 'fall':
            df['type'] = df['type'].where((df.index < max_ts) | (df.index > end_fall_ts), 'fall')
        else:
            raise ValueError(f"{type} - type is not defined - use 'rise' and 'fall'")
    else:
        if type == 'rise':
            df['type'] = df['type'].where(df.index > max_ts, 'rise')
        elif type == 'fall':
            df['type'] = df['type'].where(df.index < max_ts, 'fall')
        else:
            raise ValueError(f"{type} - type is not defined - use 'rise' and 'fall'")
    return df


def subset_df_based_on_list(df, col, lst):
    """
    this function subsets a dataframe's values from the field "col"
    when any value is in 'lst'
    :param df: the dataframe, must contain at least the column 'col'
    :param field: the string of the column name
    :param lst: the list with values according to which the dataframe will be subset
    :return:
    """
    return df[df[col].isin(lst)]


def print_complete_df(df):
    pd.set_option('display.max_rows', None)
    pd.set_option('display.max_columns', None)
    pd.set_option('display.width', None)
    print(df)
    pd.set_option('display.max_rows', 10)
    pd.set_option('display.max_columns', 5)
    pd.set_option('display.width', 80)
