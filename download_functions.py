import time

import urllib.request
import ssl
import codecs
import chromedriver_autoinstaller  # pip install chromedriver-autoinstaller
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait

try:
    from BeautifulSoup import BeautifulSoup
except ImportError:
    from bs4 import BeautifulSoup


class Download(object):

    def __init__(self):
        # setting up driver for browser download
        chromedriver_fp = chromedriver_autoinstaller.install()
        self.driver = webdriver.Chrome(chromedriver_fp)

    def download_file(self, url, out_fp):
        try:
            urllib.request.urlretrieve(url, out_fp)
            return True
        except:
            return False

    def get_site_content_as_text(self, url):
        context = ssl._create_unverified_context()
        text = urllib.request.urlopen(url, context=context).read()
        return text

    def get_site_content_as_soup(self, url):
        f = urllib.request.urlopen(url)
        raw_html = f.read()
        return BeautifulSoup(raw_html, features="html.parser")

    def get_dynamic_site_content(self, url):
        """
        reads a url with selenium with chrome to
        extract data that is generated dynamically on the website
        (urllib cannot read this)
        :param url: the url where the dynamic content is located
        :param browser_drv_fp: the location of the web browser
        :return: site content as string
        """
        self.driver.get(url)
        wait = WebDriverWait(self.driver, 10)
        wait.until(EC.visibility_of_element_located((By.XPATH, "/html/body"))) #, "/html/body"))
        time.sleep(1)  # without the sleep, text may not be fully loaded. Couldn't figure out a way to wait dynamically
        return self.driver.find_element("xpath", "/html/body").text  # https://www.geeksforgeeks.org/get-all-text-of-the-page-using-selenium-in-python/
