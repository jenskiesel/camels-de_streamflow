# object definition for data access and processing

from pathlib import Path


class State(object):
    def __init__(self, no_data):
        self.name = ''  # string: state name
        self.epsg = int  # int: source EPSG code
        self.folder = Path()  # Path: root folder where data is saved
        self.attr_folder = Path()
        self.w_folder = Path()
        self.q_folder = Path()
        self.raw_w_folder = Path()
        self.raw_q_folder = Path()
        self.gauges = []
        self.no_data = no_data


class Gauge(object):
    def __init__(self, no_data):
        self.src_id = no_data
        self.src_name = no_data
        self.add_id = no_data
        self.camels_nr = no_data  # camels id
        self.attr_url = no_data
        self.data_url = no_data
        self.w_src_fp = no_data
        self.q_src_fp = no_data
        self.w_tar_fp = no_data
        self.q_tar_fp = no_data
        self.status = no_data
        self.river = no_data
        self.area = no_data
        self.x = no_data
        self.y = no_data
        self.lat = no_data
        self.lon = no_data
        self.w_period = no_data  # water level period
        self.q_period = no_data  # discharge period
        self.q_df = None  # dataframe with discharge data
        self.w_df = None  # dataframe with water level data

    def attr_dict(self):
        # define attributes here that are part of the attribute data
        # those will be written to the attribute file
        return {'id': self.src_id,
                'add_id': self.add_id,
                'name': self.src_name,
                'status': self.status,
                'river': self.river,
                'area': self.area,
                'x': self.x,
                'y': self.y,
                'lat': self.lat,
                'lon': self.lon,
                'w_period': self.w_period,
                'q_period': self.q_period
                }

