import logging
import logging.handlers
import sys

import config as c


def prtdeb(mess_str, logger, stdout=False, stdout_fp=None):
    """
    function to make sure printouts are added to the console and logger
    :param mess_str: the message to be printed/displayed
    :param logger: the logger object
    :param stdout: if false, output is printed to console, if true, printed to file
    :param stdout_fp: file where console output is direct to (only if stdout is True)
    :return:
    """
    if c.PRINT_TO_CONSOLE:
        if stdout:
            sys.stdout = open(stdout_fp, 'a')
        print('DEBUG: ' + mess_str)
        if stdout:
            sys.stdout.flush()
            sys.stdout.close()
    logger.debug(mess_str)


def prtinf(mess_str, logger, stdout=False, stdout_fp=None):
    if c.PRINT_TO_CONSOLE:
        if stdout:
            sys.stdout = open(stdout_fp, 'a')
        print('INFO: ' + mess_str)
        if stdout:
            sys.stdout.flush()
            sys.stdout.close()
    logger.info(mess_str)


def prtwrn(mess_str, logger, stdout=False, stdout_fp=None):
    if c.PRINT_TO_CONSOLE:
        if stdout:
            sys.stdout = open(stdout_fp, 'a')
        print('WARNING: ' + mess_str)
        if stdout:
            sys.stdout.flush()
            sys.stdout.close()
    logger.warning(mess_str)


def prterr(mess_str, logger, stdout=False, stdout_fp=None):
    if c.PRINT_TO_CONSOLE:
        if stdout:
            sys.stdout = open(stdout_fp, 'a')
        print('ERROR: ' + mess_str)
        if stdout:
            sys.stdout.flush()
            sys.stdout.close()
    logger.error(mess_str)


def setup_logging(log_fp, log_name, max_bytes=100000000, backup_count=10, version=None):
    """
        Setup logging, declare file formats, log rotation

        For details see https://docs.python.org/3/howto/logging-cookbook.html#using-file-rotation

        :param log_level: Minimum message level which gets logged. Default: INFO
        :param max_bytes: Maximum size of log file before a new file is created. Default: 2MB
        :param backup_count: Number of old files to keep. Default: 5 files
    """
    if not log_fp.parent.exists():
        log_fp.parent.mkdir(parents=True, exist_ok=True)

    # Set up a specific logger with our desired output level
    base_logger = logging.getLogger(log_name)
    if c.LOG_LEVEL == 'debug':
        base_logger.setLevel(logging.DEBUG)
    elif c.LOG_LEVEL == 'info':
        base_logger.setLevel(logging.INFO)
    elif c.LOG_LEVEL == 'warning':
        base_logger.setLevel(logging.WARNING)
    elif c.LOG_LEVEL == 'error':
        base_logger.setLevel(logging.ERROR)
    else:
        raise ValueError(f'Log level {c.LOG_LEVEL} not implemented')

    # setup rotating file handler
    handler = logging.handlers.RotatingFileHandler(
        log_fp, maxBytes=max_bytes, backupCount=backup_count)

    # setup log format to print timestamp, logger name, message label and message on each line
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    # set custom formatter to handler
    handler.setFormatter(formatter)
    # Add the log message handler to the logger
    base_logger.addHandler(handler)

    # printout
    base_logger.info("--------------------------------------------------\n")
    base_logger.info(f"{log_name} created - start running workflow")
    if version is not None:
        base_logger.info(f"Version: {version}\n")

    return base_logger

