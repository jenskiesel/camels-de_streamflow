# This script is part of the workflow to process flow data for CAMELS-DE
#
#
# Author:
# Jens Kiesel - jenskiesel@gmail.com
#
#
# Python 3.6.6
import re
from pathlib import Path

import pandas as pd
import numpy as np
from pyproj import Transformer

import config as c
import log as lg
import data_objects as do
import pandas_functions as phf
import hf
from download_functions import Download


class ProcessState(object):
    """Class handles workflow"""

    def __init__(self, state_str):
        lg.prtinf(f'Processing state "{state_str}" ', c.LOG)
        self.state = do.State(c.NO_DATA)
        self.state.name = state_str
        self.state.folder = c.DATA_FOLDER / state_str
        self.state.attr_folder = self.state.folder / c.ATTR_FOLDER
        self.state.w_folder = self.state.folder / c.TARGET_DATA_DIC['w']
        self.state.q_folder = self.state.folder / c.TARGET_DATA_DIC['q']
        self.state.raw_w_folder = self.state.folder / c.TARGET_DATA_DIC['w'] / c.RAW_FOLDER
        self.state.raw_q_folder = self.state.folder / c.TARGET_DATA_DIC['q'] / c.RAW_FOLDER
        self.state.attr_folder.mkdir(parents=True, exist_ok=True)
        self.state.raw_w_folder.mkdir(parents=True, exist_ok=True)
        self.state.raw_q_folder.mkdir(parents=True, exist_ok=True)
        self.state.epsg = c.ATTR_DIC[state_str]['epsg']
        self.tar_epsg = 4326
        self.dwl = Download()  # initiate the download class

    def set_gauges(self):
        """
        function reads the GID_FILE_DIC and creates gauge objects in the state
        if a filepath (fp) is available therein, data is read from the file
        if a url is available therein, data is read from the url (not implemented yet)
        """
        lg.prtinf(f'- Populating basic gauge data', c.LOG)
        state_gid_file_info_dic = c.GID_FILE_DIC[self.state.name]
        fp = state_gid_file_info_dic['fp']
        url = state_gid_file_info_dic['url']
        sep = state_gid_file_info_dic['sep']
        clean_string = state_gid_file_info_dic['clean_string']

        if fp is not None:
            # get the data from file in folder
            if not fp.exists():
                raise FileNotFoundError(f'Gauge ID filename supplied in "GID_FILE_DIC" but file does not exist: {fp}')
            df = pd.read_csv(fp, sep=sep, engine='python')
            id_col_loc = df.columns.get_loc(state_gid_file_info_dic['id_col'])
            name_col_loc = df.columns.get_loc(state_gid_file_info_dic['name_col'])
            if state_gid_file_info_dic['add_id'] is not None:
                add_id_col_loc = df.columns.get_loc(state_gid_file_info_dic['add_id'])
            else:
                add_id_col_loc = None
            for row in df.itertuples(index=False):
                gauge = do.Gauge(self.state.no_data)
                gauge.src_id = str(row[id_col_loc])
                if clean_string:
                    gauge.src_name = hf.clean_string(row[name_col_loc])
                else:
                    gauge.src_name = row[name_col_loc]
                if add_id_col_loc is not None:
                    gauge.add_id = str(row[add_id_col_loc])
                self.state.gauges.append(gauge)
        elif url is not None:
            # get the dictionary from url
            raise NotImplementedError(f'Getting gauge-id information from url not implemented yet')
        else:
            raise ValueError(f'Either "fp" or "url" must be supplied in "GID_FILE_DIC" - currently both are "None"')


    def process_gauge_attributes(self):
        if c.ATTR_DIC[self.state.name]['url'] is None:
            lg.prtinf(f'- No attributes available for state "{self.state.name}"', c.LOG)
        else:
            lg.prtinf(f'- Processing gauge attributes available online', c.LOG)
            with open(self.state.attr_folder / c.ATTR_FP, 'w') as wf:
                for i, gauge in enumerate(self.state.gauges):
                    lg.prtdeb(f'  . {gauge.src_name}: {gauge.src_id}', c.LOG)
                    gauge.attr_url = self._get_attribute_url(gauge.src_id, gauge.src_name)
                    self._set_attributes(gauge)
                    if np.isfinite(gauge.x) and np.isfinite(gauge.y):
                        gauge.lon, gauge.lat = transform_coordinates(gauge.x, gauge.y, self.state.epsg, self.tar_epsg)
                    if i == 0:
                        wf.write(','.join(gauge.attr_dict().keys()) + '\n')
                    wf.write(','.join([str(v) for v in gauge.attr_dict().values()]) + '\n')
                    wf.flush()


    def process_gauge_data(self, process_var):
        """
        function returns a pandas dataframe of the gauge time series
        """
        data_type = c.TARGET_DATA_DIC[process_var]
        lg.prtinf(f'- Processing gauge {data_type} data', c.LOG)
        for i, gauge in enumerate(self.state.gauges):
            if self.state.name == 'SH':
                src_fn = c.SOURCE_DATA_DIC[self.state.name]['file_name'] % (gauge.src_id, gauge.src_name)
            elif self.state.name == 'HE':
                src_fn = c.SOURCE_DATA_DIC[self.state.name]['file_name'] % (gauge.src_id, process_var.upper())
            elif self.state.name == 'BW':
                src_fn = c.SOURCE_DATA_DIC[self.state.name]['file_name'] % (gauge.src_id, gauge.src_name, gauge.add_id, process_var.upper())
            else:
                raise NotImplementedError(f'Construction of source file name not implemented!')
            gauge.camels_nr = i
            tar_fn = self._get_camels_file_name(gauge)
            if process_var == 'w':
                gauge.w_src_fp = self.state.raw_w_folder / src_fn
                if not gauge.w_src_fp.exists():
                    lg.prtwrn(f'Gauge "{gauge.src_name}" is in attribute files but not available at "{gauge.w_src_fp}"!', c.LOG)
                    continue
                gauge.w_tar_fp = self.state.w_folder / tar_fn
                if c.GET_RAW_GAUGE_DATA:
                    url = self._get_data_url(gauge)
                    lg.prtdeb(url, c.LOG)
                    string = self._get_string_from_url(url)
                    self._save_string_as_file(string, gauge.w_src_fp)
                if c.CONVERT_RAW_GAUGE_DATA:
                    gauge.w_df = self._gef_df_from_file(gauge.w_src_fp, process_var)
                    self._write_df_to_file(gauge, process_var)
            elif process_var == 'q':
                gauge.q_src_fp = self.state.raw_q_folder / src_fn
                if not gauge.q_src_fp.exists():
                    lg.prtwrn(f'Gauge "{gauge.src_name}" is in attribute files but not available at "{gauge.q_src_fp}"!', c.LOG)
                    continue
                gauge.q_tar_fp = self.state.q_folder / tar_fn
                if c.GET_RAW_GAUGE_DATA:
                    url = self._get_data_url(gauge)
                    lg.prtdeb(url, c.LOG)
                    string = self._get_string_from_url(url)
                    self._save_string_as_file(string, gauge.q_src_fp)
                if c.CONVERT_RAW_GAUGE_DATA:
                    gauge.q_df = self._gef_df_from_file(gauge.q_src_fp, process_var)
                    self._write_df_to_file(gauge, process_var)
            else:
                raise ValueError(f'data_type {data_type} not known - only "w" and "q" possible')
            lg.prtdeb(f'  . {gauge.src_name} (ID {gauge.src_id})', c.LOG)


    def _get_camels_file_name(self, gauge):
        id_digits = c.TARGET_DATA_DIC['id_digits']
        return c.TARGET_DATA_DIC['file_name'] % (self.state.name,
                                                 str(gauge.camels_nr).zfill(id_digits),
                                                 gauge.src_name)

    def _gef_df_from_file(self, fp, process_var):
        """
        processes the raw file downloaded (or from the data providers) to the
        CAMELS format, including time series cleaning, remap the quality flags
        :param fp:
        :param process_var:
        :return:
        """
        sep = c.SOURCE_DATA_DIC[self.state.name]['sep']
        decimal_sep = c.SOURCE_DATA_DIC[self.state.name]['decimal_sep']
        encoding = c.SOURCE_DATA_DIC[self.state.name]['encoding']
        nskip_rows = c.SOURCE_DATA_DIC[self.state.name]['nskip']
        src_var_name = c.SOURCE_DATA_DIC[self.state.name][process_var]
        src_date_name = c.SOURCE_DATA_DIC[self.state.name]['date_name']
        src_qual_name = c.SOURCE_DATA_DIC[self.state.name]['qual_flag']
        src_date_format = c.SOURCE_DATA_DIC[self.state.name]['date_format']
        tar_qual_name = c.TARGET_DATA_DIC['qual_flag']
        tar_var_name = c.TARGET_DATA_DIC[process_var]
        tar_date_name = c.TARGET_DATA_DIC['date_name']
        quality_flag_replace_dict = c.SOURCE_DATA_DIC[self.state.name]['quality_dic']

        # check if columns are string or int-based
        if type(src_var_name) == int and type(src_date_name) == int:
            header = None
        else:
            header = 0
        try:
            df = pd.read_csv(fp, sep=sep, decimal=decimal_sep, encoding=encoding, skiprows=nskip_rows, header=header)
        except:
            lg.prtwrn(f'Cannot read file to Pandas "{fp}"', c.LOG)
            return None
        df = df.rename(columns={src_var_name: tar_var_name, src_date_name: tar_date_name,
                                src_qual_name: tar_qual_name})
        df = phf.clean_timeseries_df(df,
                                     index_col = c.TARGET_DATA_DIC['date_name'],
                                     datetime_format = src_date_format,
                                     data_cols = [tar_var_name],
                                     freq = c.TARGET_DATA_DIC['freq'],
                                     max_n_interpol = c.TARGET_DATA_DIC['interpolation_steps'],
                                     add_nodata = c.ADDITIONAL_NODATA,
                                     data_as_numeric=True)
        if df is None:
            return None
        df = phf.remove_nan_at_beginning_end(df, [tar_var_name])
        if quality_flag_replace_dict is not None:
            df[tar_qual_name] = df[tar_qual_name].replace(quality_flag_replace_dict)
        pct_nan = phf.get_percent_nan(df, tar_var_name)
        if pct_nan > c.TARGET_DATA_DIC['max_pct_miss']:
            return None
        df.index = df.index.strftime(c.TARGET_DATA_DIC['date_format'])
        return df

    def _write_df_to_file(self, gauge, process_var):
        if process_var == 'w':
            df = gauge.w_df
            out_fp = gauge.w_tar_fp
        elif process_var == 'q':
            df = gauge.q_df
            out_fp = gauge.q_tar_fp
        else:
            raise ValueError(f'Processing variable "{process_var}" not defined')
        if df is None:
            lg.prtinf(f'  . {gauge.src_name} (ID {gauge.src_id}) - contains no data!', c.LOG)
        else:
            df.to_csv(out_fp, index=True)

    def _get_data_url(self, gauge):
        """
        returns the url to access the gauge data
        """
        if self.state.name == 'SH':
            return c.RAW_DATA_ONLINE_DIC[self.state.name][process_var] % gauge.src_id
        if self.state.name == 'HE':
            return c.RAW_DATA_ONLINE_DIC[self.state.name][process_var] % gauge.add_id
        else:
            raise NotImplementedError(f'State "{self.state.name}" not implemented yet.')

    def _get_attribute_url(self, gauge_id, gauge_name=None):
        """
        constructs the url to access the attribute data
        """
        if self.state.name == 'SH':
            return c.ATTR_DIC[self.state.name]['url'] % gauge_id
        elif self.state.name == 'HE':
            return c.ATTR_DIC[self.state.name]['urlw'] % (gauge_id, gauge_name)
        else:
            raise NotImplementedError(f'State "{self.state.name}" not implemented yet.')

    def _set_attributes(self, gauge):
        """
        get gauge attributes from url
        """

        if self.state.name == 'SH':
            # get corresponding table with required attributes
            html_soup = self.dwl.get_site_content_as_soup(gauge.attr_url)
            gauge_attrs = html_soup.body.find('table', attrs={'class': 'info', 'summary': "Stammdaten und Informationen zum Pegel"}).text

            river = gauge_attrs.split('Gewässer')[1].split('\n')[1]
            try:
                gauge.river = clean_string(river)
            except:
                lg.prtwrn(f'River cannot be extracted from gauge attributes "{gauge_attrs}"', c.LOG)
                

            area = gauge_attrs.split('aktuelle Größe des oberirdischen Einzugsgebietes (AEo) in km2')[1].split('\n')[1].replace(',','.')
            try:
                gauge.area = float(area)
            except:
                lg.prtwrn(f'Area cannot be extracted from gauge attributes "{gauge_attrs}"', c.LOG)

            coords_raw = gauge_attrs.split('Koordinaten UTM')[1].split('\n')[1].split('/')
            try:
                gauge.x = int(coords_raw[0])
                gauge.y = int(coords_raw[1])
            except:
                lg.prtwrn(f'Coordinates cannot be extracted from gauge attributes "{gauge_attrs}"', c.LOG)

            data_period_raw = gauge_attrs.split('Datenbereich')[1].split('\n')[1]
            try:
                if 'Wasserstand' in data_period_raw and 'Abfluss' in data_period_raw:
                    gauge.w_period = clean_date(str(data_period_raw.split('Wasserstand:')[1].split('Abfluss:')[0]))
                    gauge.q_period = clean_date(str(data_period_raw.split('Abfluss:')[1]))
                elif 'Wasserstand' in data_period_raw and 'Abfluss' not in data_period_raw:
                    gauge.w_period = clean_date(str(data_period_raw.split('Wasserstand:')[1]))
                    gauge.q_period = c.NO_DATA
                elif 'Wasserstand' not in data_period_raw and 'Abfluss' in data_period_raw:
                    gauge.w_period = c.NO_DATA
                    gauge.q_period = clean_date(str(data_period_raw.split('Abfluss:')[1]))
                elif 'Wasserstand' not in data_period_raw and 'Abfluss' not in data_period_raw:
                    gauge.w_period = c.NO_DATA
                    gauge.q_period = c.NO_DATA
            except:
                lg.prtwrn(f'Data time period cannot be extracted from gauge attributes "{gauge_attrs}"', c.LOG)

            status_raw = gauge_attrs.split('Betriebsstatus')[1].split('\n')[1]
            try:
                gauge.status = clean_string(status_raw)
            except:
                lg.prtwrn(f'Status cannot be extracted from gauge attributes "{gauge_attrs}"', c.LOG)

        elif self.state.name == 'HE':
            text = self.dwl.get_dynamic_site_content(gauge.attr_url)
            items = text.split('\n')
            for item in items:
                if 'Flussgebiet' in item:
                    gauge.river = clean_string(item.split(' ')[1])
                elif 'Einzugsgebiet' in item:
                    try:
                        gauge.area = float(item.split(' ')[1].replace(',', '.'))
                    except:
                        lg.prtwrn(f'Area cannot be extracted from gauge attributes "{text}"', c.LOG)
                elif 'X-Koordinate' in item:
                    gauge.x = int(item.split(' ')[-1])
                elif 'Y-Koordinate' in item:
                    gauge.y = int(item.split(' ')[-1])

        else:
            raise NotImplementedError(f'State "{self.state.name}" not implemented yet.')

    def _get_string_from_url(self, url):
        if self.state.name == 'SH':
            text = self.dwl.get_site_content_as_text(url)
            text = text.decode(c.RAW_DATA_ONLINE_DIC[self.state.name]['encoding'])
            text = text.replace(c.RAW_DATA_ONLINE_DIC[self.state.name]['line_break'], '\n')
            if "Angegebene Station wurde nicht in der Datenbank gefunden" in text:
                return None
        elif self.state.name == 'HE':
            text = self.dwl.get_site_content_as_soup(url)
        else:
            raise NotImplementedError(f'Progessing state "{self.state.name}" not implemented')
        return text

    def _save_string_as_file(self, string, raw_out_fp):
        if string is not None:
            with open(raw_out_fp, 'w') as wf:
                wf.write(string)

def transform_coordinates(x, y, src_epsg, tar_epsg, always_xy=True):
    """
    function transforms the coordinates from a source EPGS code
    to a taret EPSG code
    :param x: x (or lon) coordinate (float)
    :param y: y (or lat) coordinate (float)
    :param src_epsg: source EPSG code (int)
    :param tar_epsg: target EPSG code (int)
    :param always_xy: GDAL had a swap of coordinates, requires True for newer versions
    :return: list of transformed coordinates (float, float)
    """
    inProj = f'epsg:{src_epsg}'
    outProj = f'epsg:{tar_epsg}'
    # !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    # !!! EPSG 4326 lat lon is swapped !!!
    # !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    # https://pyproj4.github.io/pyproj/stable/api/transformer.html#transformer
    transformer = Transformer.from_crs(inProj, outProj, always_xy=always_xy)
    xpr, ypr = transformer.transform(x, y)
    return [xpr, ypr]


def clean_string(s):
    return re.compile('\W+').sub('', s).strip()


def clean_date(s):
    return re.compile('[^\w.-]+').sub('', s).strip()


def remove_whitespaces(s):
    return ''.join(s.split(' '))


if __name__ == '__main__':
    #for state_str in c.PROCESS_STATES:
    state = ProcessState(c.PROCESS_STATE)
    state.set_gauges()
    if c.GET_GAUGE_ATTRIBUTES:
        state.process_gauge_attributes()
    for process_var in c.PROCESS_VARS:
        state.process_gauge_data(process_var)



