# This script is part of the workflow to process flow data for CAMELS-DE
#
#
#
#
# Author:
# Jens Kiesel - jenskiesel@gmail.com
#
#
# Python 3.6.6

import os
from pathlib import Path
import datetime

import log as lg

VERSION = 0.1

# date and time
START_TIME = datetime.datetime.now()
DATE_STR = START_TIME.strftime('%y%m%d%H%M%S')

# folder declarations
BASE_FOLDER = Path().absolute()
DATA_FOLDER = BASE_FOLDER / 'data'
INFO_FOLDER = '_info'  # subfolder within state-data folder that holds the basic info data for the gauges for download
ATTR_FOLDER = 'attributes'
RAW_FOLDER = 'raw'  # subfolder within state-data and data_type folder that holds raw data from the data providers

# output files
ATTR_FP = 'gauge_attributes.csv'  # stored in state.attribute_folder

# global variables
NO_DATA = -999  # must be a string or number as it is written to file
CPUS = min([4, os.cpu_count()])
PRINT_TO_CONSOLE = False if CPUS > 1 else True  # if true, all logs are printed to console, if false, nothing is printed to console
ADDITIONAL_NODATA = [-99, -999, -777, '']
MAX_CONSECUTIVE_NANS_INTERPOL = 0  # maximum number of no data values in time series that will be linearly interpolated
FROM_DATE = '01.01.1900'  # enter maximum possible date here is ok - it downloads the full available period then
TO_DATE = '31.12.2022'

# processing variables
# select which states are processed - must be subfolders within DATA_FOLDER for each state to be processed
PROCESS_STATE = 'SH'  # , 'HE']
# GID_FILE_DIC must be defined that holds the information of the basic gauge information either in a local
# file (or online, not implemented yet). All gauges provided in this file are further processed and the
# ids and names therein will be used to name the raw output files (if GET_RAW_GAUGE_DATA=True) or
# to find the names when data files are provided (if CONVERT_RAW_GAUGE_DATA=True)

# select which variables are processed
PROCESS_VARS = ['q', 'w']#, 'q']
# TARGET_DATA_DIC must be defined with keys = vars in the PROCESS_VARS list

# select if gauge attribues should be processed
GET_GAUGE_ATTRIBUTES = False
# STATE_ATTR_DIC must be defined that holds the information of the gauge attribute information
# that is available online (including data for the spatial reprojection) - this is only needed for a
# state if attributes are available from the data provider

# select if raw gauge data should be downloaded
GET_RAW_GAUGE_DATA = False
# downloads from url to raw_w_folder and raw_q_folder
# RAW_DATA_ONLINE_DIC must be defined with information of the data available online
# this includes the url and the properties of the online data

# select if data should be converted to the CAMELS-DE format
CONVERT_RAW_GAUGE_DATA = True
# raw data must be available from download or was directly supplied by data providers:
# uses data in the raw_w_folder and raw_q_folder to convert to CAMELS-DE format
# SOURCE_DATA_DIC and TARGET_DATA_DIC must be defined with information of the file content available in the folders
# file names must include gaugeid and gaugename defined by the data provider (not the CAMELS-DE id's and names)
# and they must correspond to the data provided in the gauge attribute file (GID_FILE_DIC)

# the GID_FILE_DIC defines the file based on which the data processing is carried out
# all gaues in the 'fp' are used to populate the gauge object and are processed (e.g. downloaded or converted
# to CAMELS format based on files obtained from the data suppliers)
GID_FILE_DIC = {'SH': {'fp': DATA_FOLDER / 'SH' / INFO_FOLDER / 'gauges_id.csv',  # file with basic gauge information
                       'url': None,  # alternatively to 'fp', the URL where the file is stored
                       'sep': ',',  # file column separator
                       'id_col': 'id',  # column string that holds the id
                       'add_id': None,  # not needed in SH
                       'name_col': 'gauge',  # column string that holds the gauge name
                       'clean_string': True,
                       },
                'HE': {'fp': DATA_FOLDER / 'HE' / INFO_FOLDER / 'hessen.txt',  # file with basic gauge information
                       'url': None,
                       # r'https://www.hlnug.de/static/pegel/wiskiweb3/data/internet/layers/20/index.json',
                       'sep': '\t',  # file column separator
                       'id_col': 'station_no',  # column string that holds the id (attribute access)
                       'add_id': 'station_id',  # column string with gauge number (data access)
                       'name_col': 'station_name',
                       'clean_string': False,
                       },  # column string that holds the gauge name
                'BW': {'fp': DATA_FOLDER / 'BW' / INFO_FOLDER / 'BW_Meta.csv',  # file with basic gauge information
                       'url': None,
                       # r'https://www.hlnug.de/static/pegel/wiskiweb3/data/internet/layers/20/index.json',
                       'sep': ',',  # file column separator
                       'id_col': 'Messstellennummer',  # column string that holds the id (attribute access)
                       'add_id': 'Gewässer',  # column string with gauge number (data access)
                       'name_col': 'Standort',
                       'clean_string': False,
                       },  # column string that holds the gauge name
                }

# this is used to download additional attribute data for the gauges (if available)
ATTR_DIC = {'SH':
                {'url': r'https://www.umweltdaten.landsh.de/pegel/jsp/pegel.jsp?mstnr=%s&wsize=free#Stammdaten',
                 'epsg': 4647
                 },
            'HE':
                {'url': r'https://www.hlnug.de/static/pegel/wiskiweb3/webpublic/#/overview/Durchfluss/station/%s/%s/stationInfoHlnug',  # % (station_id, station_name)
                 'epsg': 25832
                 },
            'BW':
                {'url': None,
                 'epsg': None
                }
            }

# defines the properties/access of the data available online - this is needed if data are downloaded
RAW_DATA_ONLINE_DIC = {'SH':
                           {'w': f'https://umweltanwendungen.schleswig-holstein.de/nuis/wafis/pegel/hydro/hy_wst_download.php?sta_no_s=%s&thema=W&von={FROM_DATE}&bis={TO_DATE}&sdatkat=null',
                            'q': f'https://umweltanwendungen.schleswig-holstein.de/nuis/wafis/pegel/hydro/hy_wst_download.php?sta_no_s=%s&thema=Q&von={FROM_DATE}&bis={TO_DATE}&sdatkat=null',
                            'encoding': 'latin-1',
                            'line_break': '\\r\\n',
                            },
                       'HE':
                           {'w': r'https://www.hlnug.de/static/pegel/wiskiweb3/data/internet/stations/0/%s/W/year.json',
                            'q': r'https://www.hlnug.de/static/pegel/wiskiweb3/data/internet/stations/0/%s/Q/year.json'
                            },  # % (station_no, Q_or_W)  string supplied by Michael Stölzle
                       'BW':
                           {'w': None,
                            'q': None
                            }
                        }  # key = State, value = URL to access data

# defines the properties of the raw downloaded files or the files received from the data suppliers
# the quality flags are currently set to 0=not quality controlled, 1=quality controlled
SOURCE_DATA_DIC = {'SH': {'w': 'Wasserstand',  # either string (=col name) or int (= column number)
                          'q': 'Abfluss',  # either string (=col name) or int (= column number)
                          'nskip': 0,
                          'date_name': 'Zeit [MEZ]',  # either string (=col name) or int (= column number)
                          'date_format': '%d.%m.%Y',
                          'qual_flag': 'Status',
                          'sep': ';',
                          'decimal_sep': ',',
                          'encoding': 'latin-1',
                          'file_name': '%s_%s.csv',  # % (gauge_id, gauge_name)
                          'quality_dic': # set to None if no quality flags are provided, else these are replaced
                                         {
                                           'nicht qualitätsgesichert': 0,
                                           'qualitätsgesichert': 1
                                         }
                          },
                    'HE': {'w': 1,  # either string (=col name) or int (= column number)
                           'q': 1,  # either string (=col name) or int (= column number)
                           'nskip': 4,
                           'date_name': 0,  # either string (=col name) or int (= column number)
                           'date_format': '%Y%m%d%H%M%S',  # https://docs.python.org/3/library/datetime.html#strftime-and-strptime-behavior
                           'qual_flag': 'Status',
                           'sep': ' ',
                           'decimal_sep': '.',
                           'encoding': 'latin-1',
                           'file_name': '%s_%s.txt',  # % (gauge_id, "W2 or "Q")
                           'quality_dic': None  # set to None if no quality flags are provided, else strings are replaced
                           },
                   'BW': {'w': 'W',  # either string (=col name) or int (= column number)
                          'q': 'Q',  # either string (=col name) or int (= column number)
                          'nskip': 3,
                          'date_name': 'Datum',  # either string (=col name) or int (= column number)
                          'date_format': '%d.%m.%Y',
                          # https://docs.python.org/3/library/datetime.html#strftime-and-strptime-behavior
                          'qual_flag': 'Geprüft (nein=ungeprüfte Rohdaten)',
                          'sep': ';',
                          'decimal_sep': ',',
                          'encoding': 'latin-1',
                          'file_name': '%s-%s-%s_%s_NHN_Tagesmittel.csv',  # % (gauge_id, add_id, "W" or "Q")
                          'quality_dic':
                                        {
                                         'nein': 0,
                                         'ja': 1
                                        }  # set to None if no quality flags are provided, else strings are replaced
                          }
                   }

# defines the CAMELS target file properties
# todo: besides the below definitions, to be discussed:
# - do we want consecutive ids? - now it is just based on the source gauge id file (consecutive)
# - the same ID for w and q?
# - the state name in the filename?
# - just keep the original id?
# - what quality control flags do we want? - for each time step? or including only most reliable data?
# - do we need additional quality control of the data?
# - unit conversion (for water level?)
# - CAMELS-GB has no separate discharge files, discharge is lumped in the timeseries with climate
TARGET_DATA_DIC = {
                    'id_digits': 4,  # number digits of target gauge id - '4' results in a four-digit zero-padded id
                    'w': 'waterlevel',
                    'q': 'discharge',
                    'w_unit': 'cm',
                    'q_unit': 'm3_s-1',
                    'date_name': 'date',
                    'qual_flag': 'quality_flag',
                    'date_format': '%Y-%m-%d',
                    'file_name': '%s%s_%s.csv',  # % (state, CAMELS_gauge_id, CAMELS_gauge_name)
                    'freq': 'D',  # pandas frequency of target time series https://pandas.pydata.org/pandas-docs/stable/user_guide/timeseries.html#timeseries-offset-aliases
                    'interpolation_steps': 0,  # time series is interpolating missing values if >0
                    'max_pct_miss': 50,  # if more than this percentage is missing, data is discarded
                   }

# logging
LOG_LEVEL = 'info'  # 'debug', 'info', 'warning', 'error'
LOG_FP = BASE_FOLDER / '#log_files' / f'{PROCESS_STATE}_data_processing_logging.log'
LOGGER_NAME = 'LOG'
LOG = lg.setup_logging(LOG_FP, LOGGER_NAME, version=VERSION)

# create folders if not existing
DATA_FOLDER.mkdir(parents=True, exist_ok=True)
